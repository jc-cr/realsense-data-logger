"""
Program to output visual plots from realsense streaming data over a sprecified time period.

Offers options for test duration, logging rate, and channels to test.
"""
from terminal_handler import set_test_params, console_log_message
import os
# from data_logger import run_test
# from data_visualizer import create_dataplots


if __name__ == "__main__":

    # Get test parameters from user
    test_parameters = set_test_params()
    
    # Create a new folder
    os.makedirs(test_parameters["resultFolderName"], exist_ok=True)

    # Run the test and log data
    #csv_filePaths = run_test(test_parameters)



    # Visualize the data
    #create_dataplots(csv_filePath)
