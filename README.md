# RealSense Camera Testing

This project contains scripts for running Intel RealSense cameras and analyzing the logged data. 

Can be used to validate USB cable data quality over time.

Code derived from Gabe Garza's realsense data loggin scripts.

## Dependencies

- Python 3.x
- pyrealsense2
- numpy
- matplotlib

## Installation

1. Clone the repository to your local machine:
2. Install the required Python libraries by running the following command:

    ```sh
    pip3 install -r requirements.txt
    ```

## Usage (WIP)

### Running the Camera Test

1. Connect the RealSense camera to your computer using the cable you want to test.

2. Run the main script to start the camera test and log data:

    ```sh
    python d435_1.py
    ```

### Analyzing and Plotting the Logged Data

1. After the main script has finished running, you can use the `dxxx_cam_logger.py` script to analyze and plot the logged data.

2. Before running `dxxx_cam_logger.py`, make sure to set the `path` variable in the script to the directory where the `cam_test.log` file is located.

3. Execute the `dxxx_cam_logger.py` script:

    ```sh
    python dxxx_cam_logger.py
    ```

    This should generate plots based on the logged data.

## Files

- `d435_1.py`: The main script that tests the RealSense camera and logs data.
- `setup_logger.py`: A script that configures logging settings.
- `dxxx_cam_logger.py`: A script for analyzing and plotting the logged data.
- `requirements.txt`: A file containing the list of dependencies required for the project.

## Note

Make sure that the RealSense camera is connected via the cable you want to test before running the scripts.