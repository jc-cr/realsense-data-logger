import os
import datetime
import time
from tqdm import tqdm

def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')


def print_table(params):
    clear_console()
    channels = ["RGB", "Depth", "Infrared"]
    print("\n-------------------------------------------------------")
    print(f"1. Test Duration (hr): {params['testDuration']}")
    print(f"2. Data Logging Interval (Log every N minute): {params['loggingInterval']}\n")

    print("Channels to Test:")
    for i, channel in enumerate(channels, start=3):
        print(
            f"{i}.    {'[*]' if params['channels'][channel] else '[ ]'} {channel}")

    print(f"\n6. Resolution Length (pixels): {params['resolution_l']}")
    print(f"7. Resolution Width (pixels): {params['resolution_w']}")
    print(f"8. FPS: {params['fps']}")

    print(f"\n9. Depth Bits: {params['depthBits']}")
    print(f"10. Color Bits: {params['colorBits']}")
    print(f"11. Infrared Bits: {params['infraredBits']}")

    print(f"\n12. Desired Result Folder Name: {params['resultFolderName']}")
    print("-------------------------------------------------------\n")


def get_new_value(parameter, expected_type):
    while True:
        try:
            new_value = expected_type(input(f"Enter new value for {parameter}: "))
            break
        except ValueError:
            print(f"Invalid input. Please enter a value of type {expected_type.__name__}")
    return new_value



def set_test_params():
  # Default parameters. Update as needed through terminal input.
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    parameters = {
        "testDuration": 0.1,
        "loggingInterval": 10,
        "channels": {"RGB": True, "Depth": True, "Infrared": True},
        "resolution_l": 640,
        "resolution_w": 480,
        "fps": 30,
        "depthBits": 16,
        "colorBits": 8,
        "infraredBits": 8,
        "resultFolderName": f"{datetime.datetime.now().date()}_TestResults_{timestamp}"
    }

    parameter_types = {
        "testDuration": float,
        "loggingInterval": int,
        "resolution_l": int,
        "resolution_w": int,
        "fps": int,
        "depthBits": int,
        "colorBits": int,
        "infraredBits": int,
        "resultFolderName": str
    }

    while True:
        print_table(parameters)
        try:
            choice = int(input("Enter the number of the desired option (0 to continue to test with above values): "))
            if choice < 0 or choice > 12:
                print("Invalid choice. Please enter a number between 0 and 12.")
                continue
        except ValueError:
            print("Invalid input. Please enter a number.")
            continue

        if choice == 0:
            break
        elif 3 <= choice <= 5:
            # Toggle the channel choice
            channel = list(parameters['channels'].keys())[choice - 3]
            parameters['channels'][channel] = not parameters['channels'][channel]
        else:
            parameter_list = ["testDuration", "loggingInterval", "", "", "", "resolution_l", "resolution_w", "fps", "depthBits", "colorBits", "infraredBits", "resultFolderName"]
            parameter = parameter_list[choice - 1]
            expected_type = parameter_types[parameter]
            new_value = get_new_value(parameter, expected_type)
            parameters[parameter] = new_value

    return parameters


def console_log_message(current_test_progress, log_messages):
    """
    Function to render a status message to the console and updating test status bar with percentage complete.
    
    current_test_progress: The elapsed time of the test so far, in seconds
    log_messages: A list of strings, each representing a separate log message
    """

    # Initialize a progress bar with a maximum value of current_test_progress
    progress_bar = tqdm(total=current_test_progress, ncols=70)

    # Print each log message on a new line
    for message in log_messages:
        # Clear the console
        clear_console()

        print(message)

    # Update the progress bar to the current test progress
    progress_bar.update(current_test_progress)

    # Close the progress bar
    progress_bar.close()
