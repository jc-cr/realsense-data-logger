import matplotlib.pyplot as plt
import numpy as np
import os

# Create object class for cam
class cam():
    
    def __init__(self):
        
        self.serial_num = None
        self.total_bandwidth = []
         
        # Depth data
        self.depth_bw = []
        self.depth_fr=[]
        self.depth_fr_cnt = []
        self.depth_fr_err = []
        self.depth_time =  []
        
        # RGB data
        self.rgb_bw = []
        self.rgb_fr=[]
        self.rgb_fr_cnt = []
        self.rgb_fr_err = []
        self.rgb_time = []
        
        # Inrared Data
        self.infrared_bw = []
        self.infrared_fr=[]
        self.infrared_fr_cnt = []
        self.infrared_fr_err = []
        self.infrared_time = []
        
#***********************
#
# INPUT PATAMETERS HERE
#
plot_log = True
plot_time = True
analyze = True
#%matplotlib qt
#**********************


# Open file  
fn = os.path.join('cam_test.log')
print('File Path', fn)
f  = open(fn)    

# Create list for data storage 
sn_list = set()
obj_list = list()    
  
# Begin reading line by line from file
for line in f:
    
    if line[0].isdigit() or line[0] == 'f' :  # lower case 'f' for L515
        data = line.split()
        
        if data[0] not in sn_list:
            
            print('Creating new object', data[0])  
            sn_list.add(data[0])
            new_cam = cam()
            new_cam.serial_num = data[0]            
            obj_list.append(new_cam)
                       
        else:
            sn = data[0]
            #print('Appending to exisiting object.', sn)

            for x in obj_list:
               
                if x.serial_num == sn:
                    #cam_obj = x
                    #print('Obj SN: ', x.serial_num)                    
        
                    # Find header info and populate the object with the correcty data         
                    if data[1] == 'Depth':                        
                         x.depth_bw.append(float(data[3]))
                         x.depth_fr.append(float(data[6]))
                         x.depth_fr_cnt.append(float(data[9]))
                         x.depth_fr_err.append(float(data[12]))
                         x.depth_time.append(float(data[14]))
                         
                    
                    elif data[1] == 'RGB':
                         x.rgb_bw.append(float(data[3]))
                         x.rgb_fr.append(float(data[6]))
                         x.rgb_fr_cnt.append(float(data[9]))
                         x.rgb_fr_err.append(float(data[12]))
                         x.rgb_time.append(float(data[14]))
                                              
                    elif data[1] == 'Inrared':
                         x.infrared_bw.append(float(data[3]))
                         x.infrared_fr.append(float(data[6]))
                         x.infrared_fr_cnt.append(float(data[9]))
                         x.infrared_fr_err.append(float(data[12]))
                         x.infrared_time.append(float(data[14]))
                         
                    elif data[1] == 'Total':
        
                        x.total_bandwidth.append(float(data[4]))
    
# Plot for analysis                         
if plot_log:   
    
    N = 0 # index number to use for normalization
    
    
    # Plot the frame error data
    fig, ax = plt.subplots(3)
    ax[0].set_title('Depth Frame Errors')
    ax[1].set_title('RGB Frame Errors')
    ax[2].set_title('Infrared Frame Errors')
    
    for val in sn_list:
        for o in obj_list:
            if o.serial_num == val:            
                # Normalize Data
                depth = np.array(o.depth_fr_err) - o.depth_fr_err[N]
                depth_t = np.array(o.depth_time) - o.depth_time[N]
                
                rgb = np.array(o.rgb_fr_err) - o.rgb_fr_err[N]
                rgb_t =  np.array(o.rgb_time) - o.rgb_time[N]   
                
                infrared = np.array(o.infrared_fr_err) - o.infrared_fr_err[N]
                inf_t =  np.array(o.infrared_time) - o.infrared_time[N]
                
                ax[0].plot(depth, label= 'SN: '+ val)           
                ax[1].plot(rgb, label= 'SN: '+ val )           
                ax[2].plot(infrared, label= 'SN: '+ val)
    plt.legend()
    ax[0].grid()
    ax[1].grid()
    ax[2].grid()
                
                
     # Plot the bandwidth data
    fig, px = plt.subplots(3)
    px[0].set_title('Depth BW')
    px[1].set_title('RGB Frame BW')
    px[2].set_title('Inrared Frame BW')
                
    for val in sn_list:
        for o in obj_list:
            if o.serial_num == val:
                
                # Normalize Data
                depth_bw = np.array(o.depth_bw) 
                depth_t = np.array(o.depth_time) - o.depth_time[N]
                rgb_bw = np.array(o.rgb_bw) 
                rgb_t =  np.array(o.rgb_time) - o.rgb_time[N]
                infrared_bw = np.array(o.infrared_bw)
                inf_t =  np.array(o.infrared_time) - o.infrared_time[N]
                
                px[0].plot(depth_bw, label= 'SN: '+ val)           
                px[1].plot(rgb_bw, label= 'SN: '+ val)           
                px[2].plot(infrared_bw, label= 'SN: '+ val)           
                
    plt.legend()
    px[0].grid()
    px[1].grid()
    px[2].grid()
    
    
     # Plot the frame rate data
    fig, fx = plt.subplots(3)
    fx[0].set_title('Depth Frame Rate')
    fx[1].set_title('RGB Frame Frame Rate')
    fx[2].set_title('Inrared Frame Frame Rate')
                
    for val in sn_list:
        for o in obj_list:
            if o.serial_num == val:
                
                # Normalize Data
                depth = np.array(o.depth_fr)
                depth_t = np.array(o.depth_time) - o.depth_time[N]
                rgb = np.array(o.rgb_fr) 
                rgb_t =  np.array(o.rgb_time) - o.rgb_time[N]
                infrared = np.array(o.infrared_fr)
                inf_t =  np.array(o.infrared_time) - o.infrared_time[N]
                
                fx[0].plot(depth, label= 'SN: '+ val)           
                fx[1].plot(rgb, label= 'SN: '+ val)           
                fx[2].plot(infrared, label= 'SN: '+ val) 
                
    plt.legend()
    fx[0].grid()
    fx[1].grid()
    fx[2].grid()
    plt.show()              
        
                
          
if plot_time:  
     # Plot the frame rate data
    fig, tx = plt.subplots(3)
    tx[0].set_title('Depth Time')
    tx[1].set_title('RGB Time')
    tx[2].set_title('Inrared Time')
      
    for val in sn_list:
        for o in obj_list:
            if o.serial_num == val:
                 if o.serial_num == val:            
                    depth_t = np.array(o.depth_time)
                    rgb_t =  np.array(o.rgb_time)               
                    inf_t =  np.array(o.infrared_time)
                    
                    tx[0].plot(depth_t, label= 'SN: '+ val)           
                    tx[1].plot(rgb_t,   label= 'SN: '+ val)           
                    tx[2].plot(inf_t,   label= 'SN: '+ val)          
    plt.legend()
    tx[0].grid()
    tx[1].grid()
    tx[2].grid()
    plt.show()                    
                                           
if analyze:
    # Plot the frame rate data
    plt.figure() 
    plt.title('Frame Error Rate')
    
    for val in sn_list:
        for o in obj_list:
            if o.serial_num == val:  
                                       
                fr_cnt = np.array(o.depth_fr_cnt)
                fr_err = np.array(o.depth_fr_err) - o.depth_fr_err[0]
                percent_error = abs(fr_err / fr_cnt)
                
                plt.plot(percent_error, label= 'SN: '+ val)  
                
                #print(fr_cnt)
                #print(fr_err)
                #print(percent_error)
    plt.legend()
    plt.grid()
    plt.show() 
                 
                   
def create_dataplots(csv_file):
    pass