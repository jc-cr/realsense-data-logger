class DeviceManger:
    
    def enumerate(self, context):
        connect_device = []

        for d in context.devices:
            if d.get_info(rs.camera_info.name).lower() != 'platform camera':
                serial = d.get_info(rs.camera_info.serial_number)
                product_line = d.get_info(rs.camera_info.product_line)
                device_info = (serial, product_line)  # (serial_number, product_line)
                connect_device.append(device_info)
                msg ='Found device: ' + d.get_info(rs.camera_info.name)+' '+d.get_info(rs.camera_info.serial_number)
                logger.info(msg)
        return connect_device


class DataLogging:

    def bandwidth(self,data_frame, type, rate):

        if type == 'depth':
            bw = (np.size(data_frame) * rate * self.depth_bits)/10e5
        elif type == 'infrared':
            bw = (np.size(data_frame) * rate * self.color_bits)/10e5
        elif type =='rgb':
            bw = (np.size(data_frame) * rate * self.infrared_bits)/10e5
        return bw

    def depth_stats(self, frames, d_cnt, d_prev_time):
        depth = frames.get_depth_frame()

        # Calculate frame rate
        d_curr_time = depth.get_timestamp() / 1000
        d_fn = depth.get_frame_number()
        d_err = d_fn - d_cnt
        d_cnt += 1
        #print 'Depth Frame number', d_fn
        d_dt = d_curr_time - d_prev_time
        try:
            d_rate = 1 / d_dt
        except:
            d_rate = 0
            pass

        bw = self.bandwidth(depth.get_data(), 'depth', d_rate)

        if not depth:
            pass
        return bw, d_cnt, d_err, d_rate, d_curr_time

    def rgb_stats(self, frames, c_cnt, c_prev_time):
        # Get color frames and count
        rgb = frames.get_color_frame()
        c_fn = rgb.get_frame_number()
        #print 'RGB Frame number', c_fn
        c_err = c_fn - c_cnt
        c_cnt += 1

        # Calculate frame rate
        c_curr_time = rgb.get_timestamp() / 1000
        c_dt = c_curr_time - c_prev_time
        try:
            c_rate = 1 / c_dt
        except:
            print('Error coolld notr calurlate c_rate')
            c_rate =0

        bw = self.bandwidth(rgb.get_data(), 'rgb', c_rate)

        if not rgb:
            pass
        return bw, c_cnt, c_err, c_rate, c_curr_time

    def infrared_stats(self, frames, i_cnt, i_prev_time):
        # Get color frames and count
        inf = frames.get_infrared_frame()
        i_fn = inf.get_frame_number()
        #print 'INF Frame number', i_fn
        i_err = i_fn - i_cnt
        i_cnt += 1

        # Calculate frame rate
        i_curr_time = inf.get_timestamp() / 1000
        i_dt = i_curr_time - i_prev_time
        try:
            i_rate = 1 / i_dt
        except:
            i_rate =0
        i_prev_time = i_curr_time
        bw = self.bandwidth(inf.get_data(), 'rgb', i_rate)

        if not inf:
            pass
        return bw, i_cnt, i_err, i_rate, i_curr_time


def run_device (device_manger):

        device_manger.device_info = device_manger.enumerate(rs.context())

        #Instantitte the data logging class
        data_logging = DataLogging()

        # Initialize the CSV writer
        csv_file = open(f"{device_info[0]}.csv", "w")
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(["Device Serial", "Data Type", "Bandwidth", "Frame Rate", "Frame Count", "Frame Errors", "Time"])

        # Create a context object. This object owns the handles to all connected realsense devices
        pipeline = rs.pipeline()
        print(device_info)
        device_serial = device_info[0]
        product_line = device_info[1]
        msg = 'Enabling Device :  ' + device_serial + ' ' + product_line
        #logger.info(msg)

        # Configure streams
        config = rs.config()
        config.enable_device(device_serial)
        config.enable_stream(rs.stream.depth,    self.length, self.width, rs.format.z16, self.fr)
        config.enable_stream(rs.stream.color,    self.length, self.width, rs.format.rgb8, self.fr)
        config.enable_stream(rs.stream.infrared, self.length, self.width, rs.format.y8, self.fr)

        # Initialize counters
        d_cnt = 0
        d_prev_time = milliseconds = int(round(time.time() * 1000))
        d_err = 0

        c_cnt = 0
        c_prev_time = milliseconds = int(round(time.time() * 1000))
        c_err = 0

        i_cnt = 0
        i_prev_time = milliseconds = int(round(time.time() * 1000))
        i_err = 0

        # Start the pipeline for the given device
        pipeline.start(config)

        # Counter for logging
        i = 0
        while not self.stop:

            try:
                # This call waits until a new coherent set of frames is available on a device
                # Calls to get_frame_data(...) and get_frame_timestamp(...) on a device will return stable values until wait_for_frames(...) is called
                frames = pipeline.wait_for_frames()

                # Process depth frames and count
                d_bw, d_cnt, d_err, d_rate, d_curr_time = self.depth_stats(frames,  d_cnt, d_prev_time)
                d_prev_time = d_curr_time

                # Process RGB frames and count
                c_bw, c_cnt, c_err, c_rate, c_curr_time = self.rgb_stats( frames, c_cnt, c_prev_time)
                c_prev_time = c_curr_time

                # Process Infrared frames and count
                i_bw, i_cnt, i_err, i_rate, i_curr_time = self.infrared_stats(frames, i_cnt, i_prev_time)
                i_prev_time = i_curr_time

                if i == self.log_N:

                    i =0

                    # Print results to screen
                    msg = ('{} Depth bandwidth: {}  frame rate: {}  frame count: {}  frame errrors: {}  time: {}')\
                        .format(device_serial, d_bw , d_rate, d_cnt, d_err, d_curr_time)
#                    logger.info(msg)


                    msg = ('{} RGB   bandwidth: {}  frame rate: {}  frame count: {}  frame errrors: {}  time: {}')\
                        .format(device_serial, c_bw, c_rate, c_cnt, c_err, c_curr_time)
#                    logger.info(msg)


                    msg = ('{} Inrared bandwidth: {} frame rate: {} frame count: {} frame errrors: {} time: {}')\
                        .format(device_serial, i_bw, i_rate, i_cnt, i_err, i_curr_time)
#                    logger.info(msg)

                    msg = ('{} Total bandwidth : {}').format(device_serial, d_bw+c_bw+i_bw)
#                    logger.info(msg)



                else:
                    i +=1

            except Exception as e:
#                logging.info(e)
                self.drops +=1
                msg = ('SN: {} dropped at {}').format(device_serial, int(round(time.time() * 1000)))
                logging.info(msg)

            except KeyboardInterrupt:
                print("KeyboardInterrupt, ending loop")

        
            realsense_data_logger = DataLogging(device_manager, devices)


        pipeline.stop()
#        logger.info('Stopping thread for SN: %s', device_serial)

def run_tests(parameters):
    device_manager = DeviceManager(parameters)
    csv_filePaths = run_device(device_manager)
    return csv_filePaths
