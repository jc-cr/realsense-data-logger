import pyrealsense2 as rs
import numpy as np
import time
import threading
import csv


class device_manager:
    """
    Class to manage the realsense devices and log data.
    """
    def __init__(self, parameters):
        """
        Initialize the device manager class with test parameters. 
        """

        self._frame_counter = 0
        self.stop = False

        # Constants
        self.length = parameters['resolution_l']
        self.width = parameters['resolution_w']
        self.depth_bits = parameters['depthBits']
        self.color_bits = parameters['colorBits']
        self.infrared_bits = parameters['infraredBits']
        self.fr = parameters['fps']
        self.drops =0
        self.duration = parameters['testDuration'] * 3600 # In Seconds
        self.log_N = self.fr * 60 * parameters['loggingInterval'] # Log every 


   



    



 

    def enable_devices(self, devices):
        # Iterate through detected devices and begin the streams
        threads = list()
        filepaths = list()

        for device_info in devices:
            x = threading.Thread(target=self.run_device, args=(device_info, ))
            threads.append(x)
            x.start()

            # Collect the filepath
            filepaths.append(x.join())

        time.sleep(self.duration)
        self.stop = True
#        logger.info('Stopping test')

        # Return the list of filepaths
        return filepaths



def run_test(parameters):
    device_manager = device_manager(parameters) # Multiply by 3600 seconds for total number of seconds to test.
    devices = device_manager.enumerate(rs.context())
    return device_manager.enable_devices(devices)
